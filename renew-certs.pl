#! /usr/bin/env perl6

use v6.c;

use Terminal::ANSIColor;

#| Renew certificates on this server, and reload services.
sub MAIN (
	#| Whether to reload/restart the system services.
	Bool:D :$reload = True
) {
	info("Running $*PROGRAM-NAME at {DateTime.now}");
	info("-" x 15);
	run « certbot renew »;
	run « systemctl restart apache2 » if $reload;
	info("-" x 15);
}

#| Send an informational message to STDOUT.
sub info (
	Str:D $message,
) {
	say color("cyan") ~ $message ~ color("reset");
}

=begin pod

=NAME    renew-certs
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=LICENSE AGPLv3

This is a small program to ease certificate updates on the DBLUG
infrastructure.

=end pod
